/**
 * File: tsh.c
 * @@@@@-@@@@@
 *
 * Your name: Sushobhan Nayak
 * Your favorite shell command: ls
 */

#include <stdbool.h>       // for bool type
#include <stdio.h>         // for printf, etc
#include <stdlib.h>        
#include <unistd.h>        // for fork
#include <string.h>        // for strlen, strcasecmp, etc
#include <ctype.h>
#include <signal.h>        // for signal
#include <sys/types.h>
#include <sys/wait.h>      // for wait, waitpid
#include <errno.h>

#include "tsh-state.h"
#include "tsh-constants.h"
#include "tsh-jobs.h"
#include "tsh-signal.h"
#include "exit-utils.h"    // provides exitIf, exitUnless

/** 
 * Block until process pid is no longer the foreground process
 */
/* static */ void waitfg(pid_t pid) {
  // provide your own implementation here
  // static keyword is commented out to suppress CT warning
  job_t *job;
  
  //Sleep as long as the job is working in the foreground
  while ((job = getjobpid(jobs, pid)) != NULL && job->state == kForeground) sleep(1);

  return;

}

/**
 * Execute the builtin bg and fg commands
 */
/* static */ void do_bgfg(char *argv[]) {
  // provide your own implementation here
  // static keyword is commented out to suppress CT warning
  
  //if no argument, nothing to do here
  if (argv == NULL || argv[1] == NULL){
    printf("%s command requires PID or %%jobid argument\n", argv[0]);
    return;
  }

  job_t *job;

  //distinguish if jid or pid
  if (argv[1][0] == '%'){ //jid
    //check validity of argument
    if (!isdigit(argv[1][1])){
      printf("%s: argument must be a PID or %%jobid\n", argv[0]);
      return;
    }
    //get job handle
    int jid = atoi((argv[1])+1);
    if ((job = getjobjid(jobs, jid)) == NULL){
      printf("%%%d: No such job\n", jid);
      return;
    }
  } else { //pid: follow a similar path
    if (!isdigit(argv[1][0])){
      printf("%s: argument must be a PID or %%jobid\n", argv[0]);
      return;
    }
    int pid = atoi(argv[1]);
    if ((job = getjobpid(jobs, pid)) == NULL){
      printf("(%d): No such process\n", pid);
      return;
    }
  }

  //Check if job exists
  if (job == NULL || job->state == kUndefined){
    printf("No job for the given PID/JID.\n");
    return;
  }

  //Run procedure for FG or BG process
  if (strcmp(argv[0], "bg") == 0){ //BG process
    if (job->state == kStopped){
      if (kill(-(job->pid), SIGCONT) < 0){
	printf("kill error: can't send SIGCONT\n");
	return;
      }
      //change job state variable
      job->state = kBackground;
      printf("[%d] (%d) %s", job->jid, job->pid, job->commandLine);
    }
  } else { // FG process
    // We know that job exists and in defined. So no test needed
      if (kill(-(job->pid), SIGCONT) < 0){
	printf("kill error: can't send SIGCONT\n");
	return;
      }
      //bring job to foreground and wait for it (Legen...dary)
      job->state = kForeground;
      waitfg(job->pid);
  }

  return;
}

/**
 * If the user has typed a built-in command then execute 
 * it immediately.  Return true if and only if the command 
 * was a builtin and executed inline.
 */
/* static */ bool handleBuiltin(char *argv[]) {
  // provide your own implementation here
  // static keyword is commented out to suppress CT warning
  if (strcmp(argv[0], "quit") == 0)  exit(0); // Nothing to do here
  //Call do_bgfg to run back/fore-ground procedures
  if ((strcmp(argv[0], "bg") == 0) || (strcmp(argv[0], "fg") == 0)){
    do_bgfg(argv);
    return true;
  }
  //list all the jobs... since this is the foreground job, all the bg jobs
  //are listed
  if (strcmp(argv[0], "jobs") == 0){
    listjobs(jobs);
    return true;
  }
  //not a built-in function
  return false;
    
}

/**
 * The kernel sends a SIGCHLD to the shell whenever a child job terminates 
 * (becomes a zombie), or stops because it receives a SIGSTOP or SIGTSTP signal.  
 * The handler reaps all available zombie children, but doesn't wait for any other
 * currently running children to terminate.  
 */
/* static */ void handleSIGCHLD(int unused) {
  // provide your own implementation here
  pid_t pid;
  int jid;
  int status;

  //wait pid in while loop to reap all completed/stopped processes
  while ((pid = waitpid(-1, &status, WNOHANG|WUNTRACED)) > 0){
    jid = pid2jid(pid);
    //delete completed jobs and print status
    if (WIFEXITED(status) || WIFSIGNALED(status)) deletejob(jobs, pid);
    if (WIFSIGNALED(status))
      printf("Job [%d] (%d) terminated by signal %d\n", jid, pid,WTERMSIG(status));
    //modify states for stopped jobs
    if (WIFSTOPPED(status)){
      job_t *job;
      if ((job = getjobpid(jobs, pid)) == NULL){
	printf("Sorry, no such job\n");
	continue;
      }
      job->state = kStopped;
      printf("Job [%d] (%d) stopped by signal %d\n", jid, pid, WSTOPSIG(status));

    }
  }
}

/**
 * The kernel sends a SIGTSTP to the shell whenever
 * the user types ctrl-z at the keyboard.  Catch it and suspend the
 * foreground job by sending it a SIGTSTP.
 */
static void handleSIGTSTP(int sig) {
  // provide your own implementation here
  //simply send signal to foreground process group
  pid_t pid = fgpid(jobs);
  if (pid > 0){
    if (kill(-pid, SIGTSTP) < 0){
      printf("Kill error\n");
      return;
    }
  }
  return;
}

/**
 * The kernel sends a SIGINT to the shell whenver the
 * user types ctrl-c at the keyboard.  Catch it and send it along
 * to the foreground job.  
 */
static void handleSIGINT(int sig) {
  // provide your own implementation here
  //simply send signal to foreground process group
  pid_t pid = fgpid(jobs);
  if (pid > 0){
    if (kill(-pid, SIGINT) < 0){
      printf("Kill error\n");
      return;
    }
  }
  return;
}

/**
 * parseline - Parse the command line and build the argv array.
 * 
 * Characters enclosed in single quotes are treated as a single
 * argument.  Return true if the user has requested a kBackground job, false if
 * the user has requested a kForeground job.  
 */
/* static */ bool parseline(const char *cmdline, char *argv[]) {
  // static keyword above is commented out to suppress CT warning about unused function
  static char array[kMaxLine];
  char *buf = array;

  strcpy(buf, cmdline);
  buf[strlen(buf)-1] = ' ';  /* replace trailing '\n' with space */
  while (*buf && (*buf == ' ')) /* ignore leading spaces */
    buf++;
  
  /* Build the argv list */
  int argc = 0;
  char *delim;
  if (*buf == '\'') {
    buf++;
    delim = strchr(buf, '\'');
  } else {
    delim = strchr(buf, ' ');
  }
  
  while (delim) {
    argv[argc++] = buf;
    *delim = '\0';
    buf = delim + 1;
    while (*buf && (*buf == ' ')) /* ignore spaces */
      buf++;
    
    if (*buf == '\'') {
      buf++;
      delim = strchr(buf, '\'');
    }
    else {
      delim = strchr(buf, ' ');
    }
  }

  argv[argc] = NULL;
  if (argc == 0) return true;  
  bool bg = *argv[argc-1] == '&';
  if (bg) argv[--argc] = NULL;
  return bg;
}

/**
 * The driver program can gracefully terminate the
 * child shell by sending it a SIGQUIT signal.
 */
static void handleSIGQUIT(int sig) {
  printf("Terminating after receipt of SIGQUIT signal\n");
  exit(1);
}

/**
 * Prints a nice little usage message to make it clear how the
 * user should be invoking tsh.
 */
static void usage() {
  printf("Usage: ./tsh [-hvp]\n");
  printf("   -h   print this message\n");
  printf("   -v   print additional diagnostic information\n");
  printf("   -p   do not emit a command prompt\n");
  exit(1);
}


/**
 * If the user has requested a built-in command (quit, jobs, bg or fg)
 * then execute it immediately.  Otherwise, fork a child process and
 * run the job in the context of the child. If the job is running in
 * the foreground, wait for it to terminate and then return.  Note:
 * each child process must have a unique process group ID so that our
 * background children don't receive SIGINT (SIGTSTP) from the kernel
 * when we type ctrl-c (ctrl-z) at the keyboard.  
 */
static void eval(char commandLine[]) {
  // provide your own implementation here
  char *argv[kMaxArgs];
  //parse command to argv
  bool bg = parseline(commandLine, argv);
  if (argv[0] == NULL) return;
  //handle built in commands
  if (handleBuiltin(argv)) return;


  pid_t pid;
  sigset_t mask;
  
  //block all the signals till job is added
  if (sigemptyset(&mask) != 0){
    printf("sigemptyset error\n");
    return;
  }
  if (sigaddset(&mask, SIGCHLD) != 0){
    printf("Signal mask adding error: SIGCHLD\n");
    return;
  }
  if (sigaddset(&mask, SIGINT) != 0){
    printf("Signal mask adding error: SIGCHLD\n");
    return;
  }
  if (sigaddset(&mask, SIGTSTP) != 0){
    printf("Signal mask adding error: SIGCHLD\n");
    return;
  }
  if (sigprocmask(SIG_BLOCK, &mask, NULL) != 0){
    printf("Signal mask error\n");
    return;
  }

  //Fork a child process
  if ((pid = fork()) < 0){
    printf("fork error\n");
    return;
  }

  /*Child process*/
  if (pid == 0){
    //Unmask signals for child
    if (sigprocmask(SIG_UNBLOCK, &mask, NULL) != 0){
      printf("Signal mask error\n");
      exit(0);
    }
    
    //Assign new process group id
    if (setpgid(0,0) < 0){
      printf("setpgid error\n");
      exit(0);
    }
    
    //Execute command
    if (execvp(argv[0], argv) < 0){
      printf("%s: Command not found\n", argv[0]);
      exit(0);
    }

  }
  
  /*Parent Process*/
  // Add child to job list
  addjob(jobs, pid, (bg ? kBackground : kForeground), commandLine);
  //Unmask masked signals
  if (sigprocmask(SIG_UNBLOCK, &mask, NULL) != 0){
    printf("Signal mask error\n");
    return;
  }
  
  //If bg process, return. Otherwise wait.
  if (bg){
    printf("[%d] (%d) %s\n", pid2jid(pid), pid, commandLine);
    return;
  }
  waitfg(pid);
  return;
    
}

/**
 * Redirect stderr to stdout (so that driver will get all output
 * on the pipe connected to stdout) 
 */
static void mergeFileDescriptors() {
  dup2(STDOUT_FILENO, STDERR_FILENO);
}

/**
 * Defines the main read-eval-print loop
 * for your simplesh.
 */
int main(int argc, char *argv[]) {
  mergeFileDescriptors();
  bool showPrompt = true;
  while (true) {
    int option = getopt(argc, argv, "hvp");
    if (option == EOF) break;
    switch (option) {
    case 'h':
      usage();
      break;
    case 'v': // emit additional diagnostic info
      verbose = true;
      break;
    case 'p':           
      showPrompt = false;
      break;
    default:
      usage();
    }
  }
  
  installSignalHandler(SIGQUIT, handleSIGQUIT); 
  installSignalHandler(SIGINT,  handleSIGINT);   // ctrl-c
  installSignalHandler(SIGTSTP, handleSIGTSTP);  // ctrl-z
  installSignalHandler(SIGCHLD, handleSIGCHLD);  // terminated or stopped child
  initjobs(jobs);  

  while (true) {
    if (showPrompt) {
      printf("%s", kPrompt);
      fflush(stdout);
    }

    char commandLine[kMaxLine];
    exitIf((fgets(commandLine, kMaxLine, stdin) == NULL) && ferror(stdin),
	   1, stdout, "fgets failed.\n");
    if (feof(stdin)) break;    
    eval(commandLine);
    fflush(stdout);
  }
  
  fflush(stdout);  
  return 0;
}
