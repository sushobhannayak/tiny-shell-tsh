/**
 * Implements a semaphore class, which isn't part of the
 * C++11 specification, but is included in Java and in posix
 * threads, and is a common enough abstraction that we should
 * provide one.
 */

#ifndef _semaphore_
#define _semaphore_

#include <mutex>
#include <condition_variable>

class semaphore {
 public:
  semaphore(int value = 0);
  void wait();
  void signal();
  
 private:
  int value;

  std::mutex m;
  std::unique_lock<std::mutex> ul;
  std::condition_variable cv;

  semaphore(const semaphore& orig) = delete;
  const semaphore& operator=(const semaphore& rhs) const = delete;
};

#endif
